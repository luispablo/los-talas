const fs = require("fs");
const configDev = JSON.parse(fs.readFileSync("config_dev.json", "utf8")).knex;
const digitalOcean = JSON.parse(fs.readFileSync("config_digitalocean.json", "utf8")).knex;

module.exports = {
  development: configDev,
  digitalocean: digitalOcean
};
