(function ()  {
	"use strict";

	var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 3000;
	var ip = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";

	console.log("Setting up server", ip, port);
	
	var configFileName = process.env.CONFIG_FILENAME || "config_dev.json";

	var server = require("./app/server")(configFileName);

	server.listen(port, ip, function () { console.log("Listening on port "+ port); });
})();
