
exports.seed = function(knex, Promise) {
	return Promise.join(
		// Deletes ALL existing entries
		knex("perfiles").del(),
		knex("usuarios").del(),

		// Inserts seed entries
		knex("perfiles").insert({id: 1, codigo: "PROPIETARIO", nombre: "Propietario", created_at: new Date()}),
		knex("perfiles").insert({id: 2, codigo: "COMISION", nombre: "Miembro de la comisión", created_at: new Date()}),
		knex("perfiles").insert({id: 3, codigo: "ADMINISTRADOR", nombre: "Administrador del barrio", created_at: new Date()}),
		knex("perfiles").insert({id: 4, codigo: "SUPERUSUARIO", nombre: "Superusuario", created_at: new Date()}),

		knex("usuarios").insert({id: 1, nombre: "root", apellido: "root", email: "-", hash: "$2a$10$gLr3.o527zmsMcCrXTsZV.iFTUttjv.JBjDcVcp0duEUqBoNdhv3O", created_at: new Date()}),

		knex("perfiles_usuario").insert({id: 1, usuario_id: 1, perfil_id: 1, created_at: new Date()})
	);
};
