"use strict";

module.exports = function (configFileName) {
	var express = require("express");
	var fs = require("fs");
	var path = require("path");
	var MultiLog = require("luispablo-multilog");
	var morgan = require("morgan");
	var bodyParser = require("body-parser");
	var TokenAuth = require("tokenauth");
	var ServerRouter = require("./ServerRouter");
	var DBAuth = require("./DBAuth");

	var TOKEN_VALID_DAYS = 7;

	var config = JSON.parse(fs.readFileSync(configFileName, "utf8"));

	// Hack for OpenShift MySQL...
	if (process.env.OPENSHIFT_MYSQL_DB_HOST) config.knex.connection.host = process.env.OPENSHIFT_MYSQL_DB_HOST;
	if (process.env.OPENSHIFT_MYSQL_DB_PORT) config.knex.connection.port = process.env.OPENSHIFT_MYSQL_DB_PORT;

	var knex = require("knex")(config.knex);

	var app = express();
	var log = MultiLog(config.multilog);
	var auth = TokenAuth(config.authentication.tokenauth, log);

	var authRoutes = auth.Router(DBAuth(knex, log), config.authentication.tokenauth.secret, TOKEN_VALID_DAYS, log);

	app.use(express.static("public"));
	app.use(morgan("dev")); // request logging
	app.use(bodyParser.json()); // for parsing application/json
	app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
	app.use("/api", ServerRouter(auth.Middleware, authRoutes, knex, config.contact, config.authentication.tokenauth.secret, log));

	// Hack to use a client router
	app.get("*", function (req, res) {
		res.sendFile(path.resolve(__dirname, "../public", "index.html"));
	});

	log.info("Using config file "+ configFileName);

	return app;
};
