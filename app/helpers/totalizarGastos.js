var _ = require("lodash");

var totalizarGastos = function totalizarGastos (gastos, conceptos) {
  var conceptosUsados = _.filter(conceptos, function (concepto) {    
    return _.some(gastos, function (gasto) {
      return gasto.concepto_id === concepto.id;
    });
  });
  return conceptosUsados.map(function (concepto) {
    return _.assign({}, concepto, {
      importeTotal: _.sumBy(_.filter(gastos, function (gasto) {
        return gasto.concepto_id === concepto.id;
      }), "importe")
    });
  });
};

module.exports = totalizarGastos;
