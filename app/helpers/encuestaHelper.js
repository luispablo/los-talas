
const calcularDatos = function calcularDatos (encuesta) {
  if (encuesta && encuesta.respuestas && encuesta.votos) {
    return encuesta.respuestas.map(r => _.assign({}, r, { 
            cantidad: encuesta.votos.filter(v => v.respuesta_id === r.id).length 
          })).sort((r1, r2) => r2.cantidad - r1.cantidad);
  } else {
    return []
  }
};

const encuestaHelper = {
  calcularDatos
};

module.exports = encuestaHelper;
