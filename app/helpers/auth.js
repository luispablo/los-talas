const _ = require("lodash");
const SUPERUSUARIO = require("./constants").SUPERUSUARIO;

const auth = function (usuario) {
  return {
    isSuperusuario: function () {
      return usuario && usuario.perfiles && _.some(usuario.perfiles, function (perfil) {
        return perfil.codigo === SUPERUSUARIO;
      }) > 0;
    }
  };
};

module.exports = auth;
