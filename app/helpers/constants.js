const constants = {
  SUPERUSUARIO: "SUPERUSUARIO",
  CURRENCY_FORMAT: "$ 0,0.00",
  NUMBER_FORMAT: "0,0.00"
};

module.exports = constants;
