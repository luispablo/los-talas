
const ENCUESTA_CHART_TYPE = "doughnut";

const getRandomColor = function getRandomColor () {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++ ) {
      color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
};

const dibujarResultadoEncuesta = function dibujarResultadoEncuesta (chartId, datos) {
  const data = {
    labels: datos.map(d => d.valor),
    datasets: [{
      data: datos.map(d => d.cantidad),
      backgroundColor: datos.map(() => chartsHelper.getRandomColor())
    }]
  };
  const options = {
    legend: { display: false }
  };
  return new Chart(chartId, { type: ENCUESTA_CHART_TYPE, data, options });
};

const chartsHelper = {
  dibujarResultadoEncuesta,
  getRandomColor
};

module.exports = chartsHelper;
