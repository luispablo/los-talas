import React from "react"; // eslint-disable-line no-unused-vars
import { IndexRedirect, Router, Route } from "react-router";
import Layout from "./ui/Layout";
import OuterLayout from "./ui/OuterLayout";
import Inicio from "./ui/Inicio";
import Login from "./ui/Login";
import Documentos from "./ui/Documentos";
import PasswordRecovery from "./ui/PasswordRecovery";
import Contacto from "./ui/Contacto";
import Admin from "./ui/admin/Admin";
import ExpensasIndex from "./ui/expensas";
import ListadoEncuestas from "./ui/encuestas/ListadoEncuestas";
import DetalleEncuesta from "./ui/encuestas/DetalleEncuesta";
import Preferencias from "./ui/Preferencias";
import { messages, Security } from "common-reducers";
import { push } from "react-router-redux";
import { requestDocumentos } from "./reducers/documentos";
import { requestNovedades } from "./reducers/novedades";
import { AuthFetch } from "tokenauth";

const { requestJWTValidation, requestNewJWT } = Security;
const { clearMessages, addErrorMessage } = messages;

const LOGIN_PATH = "/login";

const ClientRouter = function (history, store) {

  let previousPath;

  const loadData = function () {
    const jwt = store.getState().security.JWT;

    if (jwt) {
      requestDocumentos(AuthFetch(jwt), store.dispatch);
      requestNovedades(AuthFetch(jwt), store.dispatch);
    }
  };

  const requireJWT = (store) => {
    return function (nextState, replace, next) {
      const security = store.getState().security;

      if (security.JWT === null) {
        previousPath = nextState.location.pathname;
        replace(LOGIN_PATH);
        next();
      } else if (!security.validatedJWT) {
        const jwt = security.JWT;
        requestJWTValidation(jwt, fetch, store.dispatch).then(function () {
          loadData();
          next();
        }).catch(() => {
          previousPath = nextState.location.pathname;
          replace(LOGIN_PATH);
          next();
        });
      } else {
        if (window.ga) window.ga("set", "userId", security.JWT.user.id);
        next();
      }
    };
  };

  const authenticate = function () {
    const email = store.getState().login.email;
    const password = store.getState().login.password;

    requestNewJWT(email, password, fetch, store.dispatch).then(() => {
      loadData();
      store.dispatch(push(previousPath || "/"));
    }).catch(err => {
      let message = err.message;
      if (err.code === 401) message = "El usuario o la clave ingresados son inválidos";
      else if (err.code === 403) message = "Disculpe pero no posee los permisos necesarios para ingresar al sistema";
      store.dispatch(addErrorMessage(message));
      console.error(err);
    });
  };

  const clearBriefMessages = () => {
    store.dispatch(clearMessages());
  };

  history.listen( function(event) {
    if (window.ga) window.ga("send", "pageview", event.pathname);
  });

  return (
    <Router history={history}>
      <Route component={OuterLayout}>
        <Route path={LOGIN_PATH} component={Login} authenticate={authenticate} />
        <Route path="/password-recovery" component={PasswordRecovery} />
      </Route>
      <Route path="/" component={Layout} onEnter={requireJWT(store)} onChange={clearBriefMessages}>
        <IndexRedirect to="/inicio" />
        <Route path="/inicio" component={Inicio} />
        <Route path="/documentos" component={Documentos} onEnter={loadData} />
        <Route path="/contacto" component={Contacto} />
        <Route path="/encuestas" component={ListadoEncuestas} />
        <Route path="/encuestas/:id" component={DetalleEncuesta} />
        <Route path="/preferencias" component={Preferencias} />
        <Route path="/admin" component={Admin} />
        <Route path="/expensas" component={ExpensasIndex} />
      </Route>
    </Router>
  );
};

export default ClientRouter;
