import React from "react";
import { changePassword } from "../reducers/preferences";
import { connect } from "react-redux";
import { messages } from "common-reducers";

require("es6-promise").polyfill();
require("isomorphic-fetch");

const PasswordChange = function (props) {

	function change (callback) {
		return function (event) {
			callback(event.target.value);
		};
	}

	return (
		<div className="panel panel-default">
			<div className="panel-heading">
				<h3 className="panel-title">Cambio de contraseña</h3>
			</div>
			<div className="panel-body">
				<form onSubmit={props.onSubmit}>
					<div className="form-group">
						<label htmlFor="currentPassword">Contraseña actual</label>
						<input type="password" name="currentPassword" onChange={change(props.onChangeCurrent)} className="form-control" id="currentPassword" placeholder="Contraseña actual"/>
					</div>
					<div className="form-group">
						<label htmlFor="newPassword">Nueva contraseña</label>
						<input type="password" name="newPassword" onChange={change(props.onChangeNew)} className="form-control" id="newPassword" placeholder="Nueva contraseña"/>
					</div>
					<div className="form-group">
						<label htmlFor="repeatNewPassword">Repetir nueva contraseña</label>
						<input type="password" name="repeatNewPassword" onChange={change(props.onChangeRepeat)} className="form-control" id="repeatNewPassword" placeholder="Repetir nueva contraseña"/>
					</div>
					<button type="submit" className="btn btn-sm btn-default" disabled={props.buttonDisabled}>Cambiar contraseña</button>
				</form>
			</div>
		</div>
	);
};

PasswordChange.propTypes = {
	onSubmit: React.PropTypes.func,
	onChangeCurrent: React.PropTypes.func,
	onChangeNew: React.PropTypes.func,
	onChangeRepeat: React.PropTypes.func,
	buttonDisabled: React.PropTypes.bool
};

class Preferencias extends React.Component {
	constructor(props) {
		super(props);
		this.state = { currentPassword: null, newPassword: null, repeatNewPassword: null };
	}
	passwordValid () {
		return this.state.currentPassword && this.state.newPassword && this.state.repeatNewPassword && this.state.newPassword === this.state.repeatNewPassword;
	}
	changePassord (event) {
		event.preventDefault();
		this.props.clear();
		changePassword(this.props.user.username, this.state.currentPassword, this.state.newPassword, fetch).then(res => {
			if (res.status === 200) this.props.info("Contraseña cambiada con éxito");
			else if (res.status === 401) this.props.error("Contraseña actual incorrecta");
			else this.props.error(JSON.stringify(res));
		}).catch(err => this.props.info(JSON.stringify(err)));
	}
	render () {
		return (
			<div className="row">
				<div className="col-md-offset-3 col-md-6 col-xs-12">
					<PasswordChange onChangeCurrent={value => this.setState({currentPassword: value})}
											onChangeNew={value => this.setState({newPassword: value})}
											onChangeRepeat={value => this.setState({repeatNewPassword: value})}
											onSubmit={this.changePassord.bind(this)}
											buttonDisabled={!this.passwordValid()} />
				</div>
			</div>
		);
	}
}

Preferencias.propTypes = {
	user: React.PropTypes.object,
	clear: React.PropTypes.func,
	info: React.PropTypes.func,
	error: React.PropTypes.func
};

const mapStateToProps = function (store) {
	return {
		user: store.security.JWT.user
	};
};

const mapDispatchToProps = function (dispatch) {
	return {
		clear: () => dispatch(messages.clearMessages()),
		info: (message) => dispatch(messages.addInfoMessage(message)),
		error: (message) => dispatch(messages.addErrorMessage(message))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Preferencias);
