import React from "react";
import { connect } from "react-redux";
import { messages } from "common-reducers";
import { setEMail, setPassword } from "../reducers/login";
import ProgressBar from "bootstrap-progress-bar";
import { isFetching } from "common-reducers";
import { Link } from "react-router";

const Login = function (props) {

  const onSubmit = function (event) {
    event.preventDefault();
    props.clearMessages();
    props.route.authenticate();
  };

  const progressBar = props.isFetching ? <ProgressBar /> : null;

  return (
    <div className="panel panel-default">
      <div className="panel-heading">
        <h3 className="panel-title">Barrio Los Talas</h3>
      </div>
      <div className="panel-body">

        <form onSubmit={onSubmit}>
          <div className="form-group">
            <label htmlFor="email">Correo electrónico</label>
            <input type="text" value={props.email} className="form-control" id="email" autoFocus={true} placeholder="Correo electrónico" onChange={e => props.setEMail(e.target.value)}/>
          </div>
          <div className="form-group">
            <label htmlFor="password">Contraseña</label>
            <input type="password" value={props.password} className="form-control" id="password" placeholder="Contraseña" onChange={e => props.setPassword(e.target.value)}/>
          </div>

          {progressBar}

          <button className="btn btn-info">
            <span className="glyphicon glyphicon-log-in" aria-hidden="true"></span>&nbsp;
            Ingresar
          </button>&emsp;
          <Link to="/password-recovery" className="btn btn-primary">
            <span className="glyphicon glyphicon-question-sign" aria-hidden="true"></span>&nbsp;
            Olvid&eacute; mi contrase&ntilde;a
          </Link>
        </form>

      </div>
    </div>
  );
};

Login.propTypes = {
  clearMessages: React.PropTypes.func,
  route: React.PropTypes.object,
  setPassword: React.PropTypes.func
};

const mapDispatchToProps = function (dispatch) {
  return {
    clearMessages: () => dispatch(messages.clearMessages()),
    setEMail: (email) => dispatch(setEMail(email)),
    setPassword: (password) => dispatch(setPassword(password))
  };
};

const mapStateToProps = function(store) {
  return {
    email: store.login.email,
    isFetching: isFetching(store.application, "requestNewJWT"),
    password: store.login.password
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
