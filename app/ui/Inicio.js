import React from "react";
import PanelNovedad from "./PanelNovedad";
import { connect } from "react-redux";

const Inicio = function (props) {

	return (
		<div className="row">
			<div className="col-md-offset-1 col-md-7">
				<h3>Novedades</h3>

				{props.novedades.map(n => <PanelNovedad key={n.id} novedad={n} />)}
			</div>
			<div className="col-md-3">

				<div className="panel panel-default">
					<div className="panel-heading">
						<h3 className="panel-title">Horario Club House</h3>
					</div>
					<div className="panel-body">

						<strong>Martes a viernes</strong><br/>
						10:00 a 17:00 hs.<br/>
						<strong>Sábados, domingos y feriados</strong><br/>
						10:00 a 20:00 hs.<br/>
						<strong>Lunes</strong><br/>
						Cerrado

					</div>
				</div>

        <div className="panel panel-default">
					<div className="panel-body">

						<strong>C&Oacute;DIGO FACEBOOK</strong>&emsp;
            <span className="badge">{props.codigoEspecial}</span>

					</div>
				</div>

			</div>
		</div>
	);
};

Inicio.propTypes = {
  codigoEspecial: React.PropTypes.string,
	novedades: React.PropTypes.array
};

const mapStateToProps = function (store) {
	return {
    codigoEspecial: store.security.JWT && store.security.JWT.user && store.security.JWT.user.codigo_especial,
		novedades: store.novedades.items
	};
};

export default connect(mapStateToProps)(Inicio);
