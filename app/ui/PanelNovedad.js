import React from "react";
import moment from "moment";

const PanelNovedad = function (props) {
	return (
		<div className="panel panel-default">
			<div className="panel-body">
				<h4>{props.novedad.titulo} | <small>{moment(props.novedad.fecha).fromNow()}</small></h4>
				<p className="text-justify">{props.novedad.descripcion}</p>
			</div>
		</div>
	);
};

PanelNovedad.propTypes = {
	novedad: React.PropTypes.object
};

export default PanelNovedad;
