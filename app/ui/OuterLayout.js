import React from "react";
import PanelMessages from "bootstrap-feedback-panel";
import { connect } from "react-redux";

const Layout = (props) => {
  return (
    <div className="container-fluid acceso-barrio-back">
      <div className="row">
        <div className="col-md-12">&nbsp;</div>
      </div>
      <div className="row">
        <div className="col-md-offset-3 col-md-6">
          <PanelMessages messages={props.messages}/>
          {props.children}
        </div>
      </div>
    </div>
  );
};

Layout.propTypes = {
  messages: React.PropTypes.array,
  children: React.PropTypes.object
};

const mapStateToProps = function (store) {
  return {
    messages: store.messages.items
  };
};

export default connect(mapStateToProps)(Layout);
