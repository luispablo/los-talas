import React from "react";
import { connect } from "react-redux";
import { select } from "../reducers/documentos";

const LinkDocumento = function (props) {
	return (
		<li>
			<a href={props.documento.url} target="_blank">{props.documento.descripcion}</a>
		</li>
	);
};

LinkDocumento.propTypes = {
	documento: React.PropTypes.object
};

const NavDocumento = function (props) {
	const className = props.selectedDocumento && props.selectedDocumento.id === props.documento.id ? "active" : "";

	function select () {
		props.select(props.documento);
	}

	return (
		<li role="presentation" className={className}>
			<a href="#" onClick={select}>{props.documento.descripcion}</a>
		</li>
	);
};

NavDocumento.propTypes = {
	documento: React.PropTypes.object,
	select: React.PropTypes.func,
	selectedDocumento: React.PropTypes.object
};

const Documentos = function (props) {
	const navsActas = props.actas.map(function (acta) {
		return (
			<NavDocumento key={acta.id} documento={acta} selectedDocumento={props.selectedDocumento} select={props.select} />
		);
	});
	const linksActas = props.actas.map(function (acta) {
		return (
			<LinkDocumento key={acta.id} documento={acta} />
		);
	});

	const linksReglamentos = props.reglamentos.map(function (r) {
		return <LinkDocumento key={r.id} documento={r} />;
	});

	return (
		<div className="row">
			<div className="col-md-3">

				<h3>Actas</h3>

				<ul className="nav nav-pills nav-stacked hidden-xs hidden-sm">{navsActas}</ul>
				<ul className="nav nav-pills nav-stacked visible-xs visible-sm">{linksActas}</ul>

				<hr/>

				<h3>Reglamentos</h3>

				<ul className="nav nav-pills nav-stacked">{linksReglamentos}</ul>

			</div>
			<div className="col-md-9">

				<div className="embed-responsive embed-responsive-4by3 hidden-xs hidden-sm">
					{props.selectedDocumento === null ? null :
						<iframe src={props.selectedDocumento.url} className="embed-responsive-item"></iframe>
					}
				</div>

			</div>
		</div>
	);
};

Documentos.propTypes = {
	actas: React.PropTypes.array,
	documentos: React.PropTypes.array,
	reglamentos: React.PropTypes.array,
	select: React.PropTypes.func,
	selectedDocumento: React.PropTypes.object
};

const mapDispatchToProps = function (dispatch) {
	return {
		select: (documento) => dispatch(select(documento))
	};
};

const mapStateToProps = function (store) {
	return {
		actas: store.documentos.items.filter(d => d.tipo === "ACTA"),
		reglamentos: store.documentos.items.filter(d => d.tipo === "REGLAMENTO"),
		selectedDocumento: store.documentos.selected
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Documentos);
