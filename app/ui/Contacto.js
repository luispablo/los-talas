import React from "react";
import { connect } from "react-redux";
import { AuthFetch } from "tokenauth";
import ProgressBar from "bootstrap-progress-bar";
import { isFetching, messages } from "common-reducers";
import { sendQuery, setQueryText } from "../reducers/contact";

const { addInfoMessage, addErrorMessage } = messages;

const Contacto = function (props) {

	const sendQuery = function (event) {
		event.preventDefault();

		props.send(props.queryText, AuthFetch(props.jwt)).then(function () {
			props.info("Mensaje enviado con éxito");
		}).catch(function (err) {
			props.error("No se puede enviar el mensaje en este momento. Escribinos por e-mail a comisionlostalas@gmail.com");
			console.error(err);
		});
	};

	const progressBar = props.isFetching ? <ProgressBar /> : null;

	return (
		<span>
			<div className="row">
				<div className="col-md-offset-1 col-md-6">
					<h3>Para comunicarte con la comisión</h3>
					<form onSubmit={sendQuery}>
						<div className="form-group">
							<textarea value={props.queryText} onChange={e => props.setQueryText(e.target.value)} rows="10" className="form-control" id="queryText" placeholder="Escribí acá tu mensaje"></textarea>
						</div>
						{progressBar}
						<div className="form-group">
							<button className="btn btn-default" type="submit">Enviar</button>
							<label className="pull-right">o escribinos a <u>comisionlostalas@gmail.com</u></label>
						</div>
					</form>
				</div>
				<div className="col-md-4">

					<h3>Otros contactos del barrio</h3>

					<div className="list-group">
						<li className="list-group-item">
							<h4 className="list-group-item-heading">Administración</h4>
							<p className="list-group-item-text">
								<span className="fa-stack fa-lg">
									<i className="fa fa-circle fa-stack-2x"></i>
									<i className="fa fa-envelope fa-stack-1x fa-inverse"></i>
								</span>&ensp;
								lostalasdecanning@gmail.com<br/>
								<span className="fa-stack fa-lg">
									<i className="fa fa-circle fa-stack-2x"></i>
									<i className="fa fa-phone fa-stack-1x fa-inverse"></i>
								</span>&ensp;
								02320 - 403888
							</p>
						</li>
						<li className="list-group-item">
							<h4 className="list-group-item-heading">Intendencia (Ricardo Garc&iacute;a del Hoyo)</h4>
							<p className="list-group-item-text">
                <span className="fa-stack fa-lg">
									<i className="fa fa-circle fa-stack-2x"></i>
									<i className="fa fa-envelope fa-stack-1x fa-inverse"></i>
								</span>&ensp;
								intendentelostalas@gmail.com<br/>
								<span className="fa-stack fa-lg">
									<i className="fa fa-circle fa-stack-2x"></i>
									<i className="fa fa-phone fa-stack-1x fa-inverse"></i>
								</span>&ensp;
								11 2787 - 0764<br/>
							</p>
						</li>
						<li className="list-group-item">
							<h4 className="list-group-item-heading">Intendencia (Juan Rodríguez)</h4>
							<p className="list-group-item-text">
								<span className="fa-stack fa-lg">
									<i className="fa fa-circle fa-stack-2x"></i>
									<i className="fa fa-phone fa-stack-1x fa-inverse"></i>
								</span>&ensp;
								11 5175 - 3111<br/>
								<span className="fa-stack fa-lg">
									<i className="fa fa-circle fa-stack-2x"></i>
									<i className="fa fa-asterisk fa-stack-1x fa-inverse"></i>
								</span>&ensp;
								186 * 8388 (nextel)
							</p>
						</li>
						<li className="list-group-item">
							<h4 className="list-group-item-heading">Seguridad (entrada del barrio)</h4>
							<p className="list-group-item-text">
								<span className="fa-stack fa-lg">
									<i className="fa fa-circle fa-stack-2x"></i>
									<i className="fa fa-phone fa-stack-1x fa-inverse"></i>
								</span>&ensp;
								11 3547 - 6379
							</p>
						</li>
						<li className="list-group-item">
							<h4 className="list-group-item-heading">Club House - Buffet</h4>
							{/* <p className="list-group-item-text">
 								<span className="fa-stack fa-lg">
									<i className="fa fa-circle fa-stack-2x"></i>
									<i className="fa fa-whatsapp fa-stack-1x fa-inverse"></i>
								</span>&ensp;
								11 5657 - 1669 (Graciela Zammi)<br/>
								<span className="fa-stack fa-lg">
									<i className="fa fa-circle fa-stack-2x"></i>
									<i className="fa fa-whatsapp fa-stack-1x fa-inverse"></i>
								</span>&ensp;
								11 6411 - 1020 (Claudio Deniz)<br/>
								<span className="fa-stack fa-lg">
									<i className="fa fa-circle fa-stack-2x"></i>
									<i className="fa fa-whatsapp fa-stack-1x fa-inverse"></i>
								</span>&ensp;
								11 5454 - 9546 (Barbara Deniz)<br/>
							</p> */}
						</li>
					</div>

				</div>
			</div>
		</span>
	);
};

Contacto.propTypes = {
	error: React.PropTypes.func,
	fetching: React.PropTypes.func,
	info: React.PropTypes.func,
	isFetching: React.PropTypes.bool,
	jwt: React.PropTypes.object,
	messages: React.PropTypes.array,
	notFetching: React.PropTypes.func,
	queryText: React.PropTypes.string,
	send: React.PropTypes.func,
	setQueryText: React.PropTypes.func
};

const mapStateToProps = function (store) {
	return {
		isFetching: isFetching(store.application, "sendQuery"),
		jwt: store.security.JWT,
		messages: store.messages.items,
		queryText: store.contact.queryText
	};
};

const mapDispatchToProps = function (dispatch) {
	return {
		error (message) {
			dispatch(addErrorMessage(message));
		},
		info (message) {
			dispatch(addInfoMessage(message));
		},
		send: (text, fetcher) => sendQuery(text, fetcher, dispatch),
		setQueryText: (text) => dispatch(setQueryText(text))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Contacto);
