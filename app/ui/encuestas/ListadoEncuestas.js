import React from "react";
import CardEncuesta from "./CardEncuesta";

import { clearEncuestas, requestGetEncuestas } from "../../reducers/encuestas";
import { clearUsuarios, requestUsuarios } from "../../reducers/usuarios";
import { connect } from "react-redux";
import { AuthFetch } from "tokenauth";

class ListadoEncuestas extends React.Component {
  componentDidMount () {
    const fetcher = AuthFetch(this.props.jwt);
    this.props.reloadEncuestas(fetcher);
    this.props.reloadUsuarios(fetcher);
  }
  render () {
    return (
      <div>
        {this.props.encuestas.map(function (encuesta) {
          return <CardEncuesta  key={encuesta.id} encuesta={encuesta} 
                                usuarios={this.props.usuariosActivos} />;
        }.bind(this))}
      </div>
    );
  }
}

const mapStateToProps = function mapStateToProps (store) {
  return {
    encuestas: store.encuestas.items,
    jwt: store.security.JWT,
    usuariosActivos: store.usuarios.items.filter(u => !u.fecha_baja)
  };
};

const mapDispatchToProps = function mapDispatchToProps (dispatch) {
  return {
    reloadEncuestas (fetcher) {
      dispatch(clearEncuestas());
      requestGetEncuestas(fetcher, dispatch);
    },
    reloadUsuarios (fetcher) {
      dispatch(clearUsuarios());
      requestUsuarios(fetcher, dispatch);
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListadoEncuestas);
