import React from "react";
import moment from "moment";
import numeral from "numeral";
import Chart from "chart.js";
import chartsHelper from "../../helpers/chartsHelper";
import encuestaHelper from "../../helpers/encuestaHelper";
import constantes from "../../helpers/constantes";

import { Link } from "react-router";

const ENCUESTA_CHART_ID = "chartEncuesta";

const buildChartId = function buildChartId (encuesta) {
  return `${ENCUESTA_CHART_ID}_${encuesta.id}`;
};

class CardEncuesta extends React.Component {
  constructor (props) {
    super(props);
    this.state = { chart: null };
  }
  componentDidMount () {
    const encuesta = this.props.encuesta;
    const estaCerrada = encuesta.estado === constantes.ENCUESTA.ESTADOS.CERRADA;
    const votos = this.props.encuesta.votos || [];
    const respuestas = this.props.encuesta.respuestas || [];

    if (estaCerrada && votos && votos.length > 0) {
      if (this.state.chart) this.state.chart.destroy();
      const chartId = buildChartId(this.props.encuesta);
      const datos = encuestaHelper.calcularDatos(this.props.encuesta);
      this.setState({ chart: chartsHelper.dibujarResultadoEncuesta(chartId, datos) });
    }
  }
  componentWillReceiveProps (nextProps) {
    const encuesta = this.props.encuesta;
    const estaCerrada = encuesta.estado === constantes.ENCUESTA.ESTADOS.CERRADA;
    const votos = nextProps.encuesta.votos || [];
    const respuestas = nextProps.encuesta.respuestas || [];

    if (estaCerrada && votos && votos.length > 0) {
      if (this.state.chart) this.state.chart.destroy();
      const chartId = buildChartId(nextProps.encuesta);
      const datos = encuestaHelper.calcularDatos(this.props.encuesta);
      this.setState({ chart: chartsHelper.dibujarResultadoEncuesta(chartId, datos) });
    }
  }
  render () {
    const encuesta = this.props.encuesta;
    const chartId = buildChartId(encuesta);
    const estaCerrada = encuesta.estado === constantes.ENCUESTA.ESTADOS.CERRADA;
    const fechaFin = moment(encuesta.fecha_fin).format("dddd D MMMM YYYY");
    const votos = encuesta.votos;
    const participacion = votos && (votos.length / this.props.usuarios.length);
    const datos = encuestaHelper.calcularDatos(encuesta);

    return (
      <div className="col-md-6">
        <div className="panel panel-primary">
          <div className="panel-heading">{encuesta.titulo}</div>
          <div className="panel-body">
            <div className="row">
              <div className="col-md-8">
                <p dangerouslySetInnerHTML={{__html: encuesta.resumen}} />
              </div>
              <div className="col-md-4 text-center">
                <canvas id={chartId} width="1" height="1"></canvas>
              </div>
            </div>
          </div>
          <div className="panel-footer">
            { estaCerrada ? 
              <div className="row">
                <div className="col-md-8">
                  Respuesta ganadora: <strong>{datos[0].valor}</strong>
                </div>
                <div className="col-md-4 text-right">
                  <Link to={`/encuestas/${encuesta.id}`} className="btn btn-primary btn-xs">&nbsp;Ver detalle&nbsp;</Link>
                </div>
              </div> : 
              <div className="row">
                <div className="col-md-8">
                  Votaci&oacute;n abierta hasta el <strong>{fechaFin}</strong>
                </div>
                <div className="col-md-4 text-right">
                  <Link to={`/encuestas/${encuesta.id}`} className="btn btn-info btn-xs">&nbsp;Votar&nbsp;</Link>
                </div>
              </div>
            }
          </div>
        </div>
      </div>
    );
  }
}

export default CardEncuesta;
