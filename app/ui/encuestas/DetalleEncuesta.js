import React from "react";
import _ from "lodash";
import constantes from "../../helpers/constantes";
import Chart from "chart.js";
import chartsHelper from "../../helpers/chartsHelper";
import encuestaHelper from "../../helpers/encuestaHelper";
import numeral from "numeral";

import { connect } from "react-redux";
import { requestVotar } from "../../reducers/encuestas";
import { AuthFetch } from "tokenauth";

const CHART_ID = "resultadoChart";

class PanelResultado extends React.Component {
  constructor (props) {
    super(props);
    this.state = { chart: null };
  }
  componentDidMount () {
    const datos = encuestaHelper.calcularDatos(this.props.encuesta);

    if (datos && datos.length > 0) {
      if (this.state.chart) this.state.chart.destroy();
      this.setState({ chart: chartsHelper.dibujarResultadoEncuesta(CHART_ID, datos) });
    }
  }
  render () {
    const encuesta = this.props.encuesta;
    const datos = encuestaHelper.calcularDatos(encuesta);
    const totalVotos = encuesta.votos && encuesta.votos.length || 0;

    return (
      <div className="panel panel-primary">
        <div className="panel-heading">Resultado</div>
        <div className="panel-body">
          <canvas id={CHART_ID}></canvas>
        </div>
        <ul className="list-group">
          {datos.map(function (d, i) {
            const porcentaje = d.cantidad / totalVotos;

            return (
              <li key={d.id} className={`list-group-item ${i === 0 ? "winner" : null}`}>
                {d.valor} <span className="badge">{numeral(porcentaje).format("0 %")}</span>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}

const PanelVotacion = function PanelVotacion (props) {
  const encuesta = props.encuesta;
  const idUsuario = props.jwt.user.id;
  const voto = _.find(encuesta.votos || [], { usuario_id: idUsuario });

  return (
    <div className="panel panel-primary">
      <div className="panel-heading">Tu voto</div>
      <div className="panel-body">
        {encuesta.respuestas.sort((r1, r2) => r1.orden - r2.orden).map(function (respuesta) {
          const checked = voto && respuesta.id === voto.respuesta_id;

          return (
            <div className="radio" key={respuesta.id}>
              <label>
                <input  type="radio" name="voto" value={respuesta.id} checked={checked}
                        onChange={e => props.votar(encuesta, respuesta, props.jwt)} />
                <p className={respuesta.css_class}>{respuesta.valor}</p>
              </label>
            </div>
          );
        })}
      </div>
    </div>
  );
};

const DetalleEncuesta = function DetalleEncuesta (props) {
  const encuesta = _.find(props.encuestas, { id: parseInt(props.params.id) });
  const estaCerrada = encuesta.estado === constantes.ENCUESTA.ESTADOS.CERRADA;

  return (
    <div>
      <div className="row">
        <div className="col-md-offset-1 col-md-10">
          <h1>{encuesta.titulo}</h1>
          <p className="lead" dangerouslySetInnerHTML={{__html: encuesta.resumen}} />
          <br/>
        </div>
      </div>
      <div className="row">
        <div className="col-md-offset-1 col-md-7">
          <p dangerouslySetInnerHTML={{__html: encuesta.detalle}} />
        </div>
        <div className="col-md-3">
          {estaCerrada ?
            <PanelResultado encuesta={encuesta} /> :
            <PanelVotacion encuesta={encuesta} votar={props.votar} jwt={props.jwt} />
          }
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = function mapStateToProps (store) {
  return {
    encuestas: store.encuestas.items,
    jwt: store.security.JWT
  };
};

const mapDispatchToProps = function mapDispatchToProps (dispatch) {
  return {
    votar (encuesta, respuesta, jwt) {
      requestVotar (encuesta, respuesta, jwt.user.id, AuthFetch(jwt), dispatch).then(function () {
        console.log("done");
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DetalleEncuesta);
