import React from "react";
import _ from "lodash";
import numeral from "numeral";
import moment from "moment";
import constantes from "../../helpers/constants";

const STYLE_NO_WRAP = { whiteSpace: "nowrap" };
const STYLE_NUMBER = { whiteSpace: "nowrap", textAlign: "right" };

const RowGasto = function (props) {
  const gasto = props.gasto;
  const concepto = _.find(props.conceptos, { id: gasto.concepto_id });
  const proveedor = _.find(props.proveedores, { id: gasto.proveedor_id });

  return (
    <tr className="small">
      <td style={STYLE_NO_WRAP}>{concepto && concepto.nombre || "-"}</td>
      <td>{proveedor && proveedor.nombre || "-"}</td>
      <td>{gasto.detalle}</td>
      <td style={STYLE_NUMBER}>{numeral(gasto.importe).format(constantes.CURRENCY_FORMAT)}</td>
      <td>&nbsp;</td>
    </tr>
  );
};

export default RowGasto;