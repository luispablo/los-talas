import React from "react";
import moment from "moment";
import TablaGastos from "./TablaGastos";
import DonaGastosMes from "./DonaGastosMes";
import { connect } from "react-redux";
import { AuthFetch } from "tokenauth";
import {
  requestConceptos,
  requestDeleteGasto,
  requestInsertGasto,
  requestGastosLiquidacion,
  requestLiquidaciones,
  requestProveedores,
  requestUpdateGasto
} from "../../reducers/expensas";
import auth from "../../helpers/auth";
import _ from "lodash";

const sortBy = function sortBy (orderBy, conceptos, proveedores) {
  const direction = orderBy.indexOf("ASC") >= 0 ? -1 : 1;
  const porImporte = orderBy.indexOf("importe") >= 0;
  const porConcepto = orderBy.indexOf("concepto") >= 0;
  const porProveedor = orderBy.indexOf("proveedor") >= 0;

  return function (gasto1, gasto2) {
    if (porImporte) {
      return (gasto1.importe < gasto2.importe ? -1 : 1) * direction;
    } else if (porConcepto) {
      const concepto1 = _.find(conceptos, { id: gasto1.concepto_id });
      const concepto2 = _.find(conceptos, { id: gasto2.concepto_id });
      return (concepto1 && concepto2 && concepto1.nombre < concepto2.nombre ? -1 : 1) * direction;
    } else if (porProveedor) {
      const proveedor1 = _.find(proveedores, { id: gasto1.proveedor_id });
      const proveedor2 = _.find(proveedores, { id: gasto2.proveedor_id });
      return (proveedor1 && proveedor2 && proveedor1.nombre < proveedor2.nombre ? -1 : 1) * direction;
    } else {
      return -1;
    }
  };
};

const filterSortGastos = function filterSortGastos (params) {
  if (params.liquidacionId) {
    return params.gastos.filter(gasto => gasto.liquidacion_id === params.liquidacionId)
                        .sort(sortBy(params.orderBy, params.conceptos, params.proveedores));
  } else {
    return [];
  }
};

class ExpensasIndex extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      selectedLiquidacionId: -1,
      orderBy: "importe ASC"
    };
    this.handleSelect = this.handleSelect.bind(this);
    this.handleDeleteGasto = this.handleDeleteGasto.bind(this);
    this.handleInsertGasto = this.handleInsertGasto.bind(this);
    this.handleUpdateGasto = this.handleUpdateGasto.bind(this);
  }
  componentDidMount () {
    this.props.initialLoad(this.props.jwt).then(function () {
      if (this.props.liquidaciones.length > 0) this.handleSelect(this.props.liquidaciones[0].id);
    }.bind(this));
  }
  handleSelect (idLiquidacion) {
    this.setState({selectedLiquidacionId: idLiquidacion});
    if (!_.some(this.props.gastos, { liquidacion_id: idLiquidacion })) this.props.cargarGastos(this.props.jwt, idLiquidacion);
  }
  handleDeleteGasto (idGasto) {
    this.props.deleteGasto(this.props.jwt, idGasto);
  }
  handleInsertGasto (gasto) {
    this.props.insertGasto(this.props.jwt, gasto);
  }
  handleUpdateGasto (gasto) {
    this.props.updateGasto(this.props.jwt, gasto);
  }
  render () {
    const authHelper = auth(this.props.jwt.user);
    const that = this;
    const gastos = filterSortGastos ({
      liquidacionId: this.state.selectedLiquidacionId,
      gastos: this.props.gastos,
      conceptos: this.props.conceptos,
      proveedores: this.props.proveedores,
      orderBy: this.state.orderBy
    });
    const liquidacion = this.state.selectedLiquidacionId ? _.find(this.props.liquidaciones, { id: this.state.selectedLiquidacionId }) : null;

    return (
      <div className="row">
        <div className="col-md-3">

          <div className="panel panel-default">
            <div className="panel-body">

              <form>
                <div className="form-group">
                  <label>Liquidaci&oacute;n </label>
                    <select className="form-control text-capitalize" value={this.state.selectedLiquidacionId} 
                            onChange={e => this.handleSelect(parseInt(e.target.value))}>
                      {this.props.liquidaciones.map(function (liquidacion) {
                        return (
                          <option key={liquidacion.id} value={liquidacion.id} className="text-capitalize">
                            {moment([liquidacion.anio, liquidacion.mes - 1]).format("MMMM YYYY")}
                          </option>
                        );
                      })}
                    </select>
                </div>
              </form>

            </div>
          </div>

          <DonaGastosMes gastos={gastos} conceptos={this.props.conceptos} />

        </div>
        <div className="col-md-9">
      
            <TablaGastos  editable={authHelper.isSuperusuario()} liquidacion={liquidacion} 
                          gastos={gastos} conceptos={this.props.conceptos} orderBy={this.state.orderBy}
                          proveedores={this.props.proveedores} onReorder={orderBy => this.setState({orderBy})}
                          onInsertGasto={this.handleInsertGasto}
                          onDeleteGasto={this.handleDeleteGasto}
                          onUpdateGasto={this.handleUpdateGasto} />

        </div>
      </div>
    );
  }
}

const mapStateToProps = function (store) {
  return {
    conceptos: store.expensas.conceptos.sort((c1, c2) => c1.nombre < c2.nombre ? -1 : 1),
    gastos: store.expensas.gastos,
    jwt: store.security.JWT,
    liquidaciones: store.expensas.liquidaciones.sort((l1, l2) => l1.anio > l2.anio || l1.anio === l2.anio && l1.mes > l2.mes ? -1 : 1),
    proveedores: store.expensas.proveedores.sort((p1, p2) => p1.nombre < p2.nombre ? -1 : 1)
  };
};

const mapDispatchToProps = function (dispatch) {
  return {
    initialLoad (jwt) {
      const fetcher = AuthFetch(jwt);
      requestProveedores(fetcher, dispatch);
      return requestConceptos(fetcher, dispatch)
              .then(() => requestLiquidaciones(fetcher, dispatch));
    },
    cargarGastos (jwt, idLiquidacion) {
      requestGastosLiquidacion(idLiquidacion, AuthFetch(jwt), dispatch);
    },
    deleteGasto (jwt, idGasto) {
      return requestDeleteGasto(idGasto, AuthFetch(jwt), dispatch);
    },
    insertGasto (jwt, gasto) {
      return requestInsertGasto(gasto, AuthFetch(jwt), dispatch);
    },
    updateGasto (jwt, gasto) {
      requestUpdateGasto(gasto, AuthFetch(jwt), dispatch);
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ExpensasIndex);
