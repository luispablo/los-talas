import React from "react";
import _ from "lodash";
import numeral from "numeral";
import moment from "moment";
import constantes from "../../helpers/constants";
import RowGasto from "./RowGasto";
import RowGastoEditable from "./RowGastoEditable";
import { SortableTH } from "react-bootstrap3-components";

const STYLE_NO_WRAP = { whiteSpace: "nowrap" };
const STYLE_NUMBER = { whiteSpace: "nowrap", textAlign: "right" };

const TablaGastos = function (props) {
  const liquidacion = props.liquidacion;

  return (
    <div className="panel panel-default">
      <table className="table table-striped table-hover">
        <thead>
          <tr>
            <SortableTH label="Concepto" field="concepto" orderBy={props.orderBy} onClick={props.onReorder} />
            <SortableTH label="Proveedor" field="proveedor" orderBy={props.orderBy} onClick={props.onReorder} />
            <th>Detalle</th>
            <SortableTH label="Importe" field="importe" orderBy={props.orderBy} onClick={props.onReorder} />
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {liquidacion && props.editable ?
            <RowGastoEditable key={-1} gasto={{ liquidacion_id: liquidacion.id, detalle: "-", importe: 0 }} 
                              conceptos={props.conceptos} proveedores={props.proveedores}
                              onUpdate={props.onInsertGasto} />
            : null
          }
          {props.gastos.map(function (gasto) {
            if (props.editable) {
              return <RowGastoEditable key={gasto.id} gasto={gasto} conceptos={props.conceptos} 
                                        proveedores={props.proveedores}
                                        onDelete={props.onDeleteGasto} onUpdate={props.onUpdateGasto} />;
            } else {
              return <RowGasto  key={gasto.id} gasto={gasto} conceptos={props.conceptos} 
                                proveedores={props.proveedores} />;
            }
          })}
        </tbody>
        <tfoot>
          <tr>
            <td colSpan="3"><strong>{props.gastos.length} items por un total de</strong></td>
            <td style={STYLE_NUMBER}>
              <strong>
                {numeral(_(props.gastos.map(g => g.importe)).sum()).format(constantes.CURRENCY_FORMAT)}
              </strong>
            </td>
            <td>&nbsp;</td>
          </tr>
        </tfoot>
      </table>
    </div>
  );
};

export default TablaGastos;
