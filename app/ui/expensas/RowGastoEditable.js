import React from "react";
import _ from "lodash";

class RowGastoEditable extends React.Component {
  constructor (props) {
    super(props);
    
    const firstConceptoId = props.conceptos.length > 0 ? props.conceptos[0].id : 0;
    const firstProveedorId = props.proveedores.length > 0 ? props.proveedores[0].id : 0;

    this.state = {
      id: props.gasto.id,
      liquidacion_id: props.gasto.liquidacion_id,
      concepto_id: props.gasto.concepto_id || firstConceptoId,
      proveedor_id: props.gasto.proveedor_id || firstProveedorId,
      detalle: props.gasto.detalle,
      importe: props.gasto.importe
    };
    this.isDirty = this.isDirty.bind(this);
    this.undo = this.undo.bind(this);
    this.handleSave = this.handleSave.bind(this);
  }
  componentWillReceiveProps (newProps) {
    this.setState({ liquidacion_id: newProps.gasto.liquidacion_id });
  }
  handleSave () {
    this.props.onUpdate(this.state);
    if (!this.state.id) this.setState({ concepto_id: 0, proveedor_id: 0, detalle: "", importe: "" });
    document.getElementById(this.conceptoSelectId()).focus();
  }
  isDirty () {
    return this.props.gasto.concepto_id !== this.state.concepto_id
            || (this.props.gasto.proveedor_id !== this.state.proveedor_id && this.state.proveedor_id !== 0)
            || this.props.gasto.detalle !== this.state.detalle
            || this.props.gasto.importe !== this.state.importe;
  }
  undo () {
    this.setState({
      concepto_id: this.props.gasto.concepto_id || -1,
      proveedor_id: this.props.gasto.proveedor_id || -1,
      detalle: this.props.gasto.detalle,
      importe: this.props.gasto.importe
    });
  }
  conceptoSelectId () {
    return `concepto_id_${this.state.id ? this.state.id : "new"}`;
  }
  render () {
    const concepto = _.find(this.props.conceptos, { id: this.state.concepto_id });
    const proveedor = _.find(this.props.proveedores, { id: this.state.proveedor_id });

    return (
      <tr className={this.state.id ? (this.isDirty() ? "warning" : "") : "info"}>
        <td width="20%">
          <select className="form-control" value={this.state.concepto_id} id={this.conceptoSelectId()}
                  onChange={e => this.setState({ concepto_id: parseInt(e.target.value) })}>
            {this.props.conceptos.map(function (concepto) {
              return <option key={concepto.id} value={concepto.id}>{concepto.nombre}</option>;
            })}
          </select>
        </td>
        <td width="20%">
          <select className="form-control" value={this.state.proveedor_id} id="proveedor_id"
                  onChange={e => this.setState({ proveedor_id: parseInt(e.target.value) })}>
            {this.props.proveedores.map(function (proveedor) {
              return <option key={proveedor.id} value={proveedor.id}>{proveedor.nombre}</option>;
            })}
          </select>
        </td>
        <td>
          <input  type="text" className="form-control" value={this.state.detalle} 
                  onChange={e => this.setState({ detalle: e.target.value })} />
        </td>
        <td width="15%">
          <input  type="number" className="form-control text-right" value={this.state.importe} 
                  onChange={e => this.setState({ importe: parseFloat(e.target.value) })} />
        </td>
        <td width="10%">
          <div className="btn-group" role="group">
            {this.isDirty() ?
              <button className="btn btn-success btn-xs" type="button" onClick={this.handleSave}>
                <span className="glyphicon glyphicon-ok" aria-hidden="true"></span>
              </button>
              : null
            }
            {this.isDirty() ?
              <button className="btn btn-default btn-xs" type="button" onClick={this.undo}><span className="glyphicon glyphicon-refresh" aria-hidden="true"></span></button>
            : null}
            {this.state.id ? 
              <button className="btn btn-danger btn-xs" type="button" onClick={() => this.props.onDelete(this.props.gasto.id)}><span className="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
              : null
            }
          </div>
        </td>
      </tr>
    );
  }
}

export default RowGastoEditable;