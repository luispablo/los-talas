import React from "react";
import Chart from "chart.js";
import _ from "lodash";
import totalizarGastos from "../../helpers/totalizarGastos";
import numeral from "numeral";
import constantes from "../../helpers/constants";
import chartsHelper from "../../helpers/chartsHelper";

const CHART_ID = "gastosMesChart";

const calcularDatos = function calcularDatos (gastos, conceptos) {
  return totalizarGastos(gastos, conceptos)
          .map(t => _.assign({}, t, { color: chartsHelper.getRandomColor(), importeTotal: Math.floor(t.importeTotal * 100) / 100 }))
          .sort((t1, t2) => t1.importeTotal > t2.importeTotal ? -1 : 1);
};

const dibujar = function dibujar (gastos, totales) {
  const ctx = document.getElementById(CHART_ID).getContext("2d");
  const data = {
    labels: totales.map(t => t.nombre),
    datasets: [{
      data: totales.map(t => t.importeTotal),
      backgroundColor: totales.map(t => t.color)
    }]
  };
  const options = {
    legend: { display: false }
  };
  return new Chart(ctx, { type: "doughnut", data, options });
};

class DonaGastosMes extends React.Component {
  constructor (props) {
    super(props);
    this.state = { 
      chart: null, totales: []
    };
  }
  componentDidMount () {
    if (this.props.gastos.length && this.props.conceptos.length) {
      if (this.state.chart) this.state.chart.destroy();
      const totales = calcularDatos(this.props.gastos, this.props.conceptos);
      this.setState({ 
        totales: totales,
        chart: dibujar(this.props.gastos, totales) 
      });
    }
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.gastos.length && nextProps.conceptos.length) {
      if (this.state.chart) this.state.chart.destroy();
      const totales = calcularDatos(nextProps.gastos, nextProps.conceptos);
      this.setState({ 
        totales: totales,
        chart: dibujar(nextProps.gastos, totales) 
      });
    }
  }
  render () {
    return (
      <div className="panel panel-default">
        <div className="panel-heading">
          Totales
        </div>
        <div className="panel-body">
          <canvas id={CHART_ID} width="400" height="400"></canvas>
        </div>
        <table className="table">
          <tbody>
            {this.state.totales.map(function (total) {
              return (
                <tr key={total.id}>
                  <td><span className="badge" style={{ backgroundColor: total.color }}>&nbsp;</span></td>
                  <td>{total.nombre}</td>
                  <td className="text-right nowrap">{numeral(total.importeTotal).format(constantes.NUMBER_FORMAT)}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default DonaGastosMes;