import React from "react"; // eslint-disable-line no-unused-vars
import { messages } from "common-reducers";
import { connect } from "react-redux";

const PasswordRecovery = function (props) {
  let email = "";

  const doReset = function (event) {
    event.preventDefault();
    fetch("/api/usuarios/reset-password", {
      method: "PUT",
      headers: { "Accept": "application/json", "Content-Type": "application/json" },
      body: JSON.stringify({ email: email })
    }).then(function (res) {
      if (res.status === 200) props.info("Le hemos enviado una nueva contraseña a su casilla de correo");
      else if (res.status === 404) props.error("No se encontró ningún usuario con el mail "+ email);
      else props.info(res.statusText);
    })
  };

  return (
    <div className="row">
      <div className="col-md-offset-2 col-md-8">

        <div className="panel panel-default">
          <div className="panel-heading">
            <h3 className="panel-title">Olvid&eacute; mi contrase&ntilde;a</h3>
          </div>
          <div className="panel-body">
            <form onSubmit={doReset}>
              <div className="form-group">
                <label htmlFor="email">Correo electr&oacute;nico</label>
                <input type="email" className="form-control" onChange={e => email = e.target.value} id="email" />
              </div>
              <div className="form-group">
                <button type="submit" className="btn btn-primary">
                  <span className="glyphicon glyphicon-refresh" aria-hidden="true"></span>&nbsp;
                  Solicitar nueva contraseña
                </button>
              </div>
            </form>
          </div>
        </div>

      </div>
    </div>
  );
};

const mapStateToProps = function (store) {
  return {
  };
};

const mapDispatchToProps = function (dispatch) {
  return {
    info: message => dispatch(messages.addInfoMessage(message)),
    error: message => dispatch(messages.addErrorMessage(message))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PasswordRecovery);
