import React from "react";
import { Link } from "react-router";
import { connect } from "react-redux";
import { Security } from "common-reducers";
import { push } from "react-router-redux";
import auth from "../helpers/auth";

const { requestJWTRevoke } = Security;

const MenuItem = function (props) {
	const className = props.currentPath.indexOf(props.path) >= 0 ? "active" : "";
	return <li className={className}><Link to={props.path}>{props.label}</Link></li>;
};

MenuItem.propTypes = {
	currentPath: React.PropTypes.string,
	path: React.PropTypes.string,
	label: React.PropTypes.string
};

const Menu = function (props) {
  const authHelper = auth(props.JWT.user);
	const username = props.JWT.user.username;
	const logout = () => props.requestJWTRevoke(props.JWT).then(() => props.redirect());
	const currentPath = props.routing.locationBeforeTransitions.pathname;
	const menuItems = [
		<MenuItem key="expensas" currentPath={currentPath} path="/expensas" label="Expensas" />,
		// <MenuItem key="encuestas" currentPath={currentPath} path="/encuestas" label="Encuestas" />,
		<MenuItem key="documentos" currentPath={currentPath} path="/documentos" label="Documentos" />,
		<MenuItem key="contacto" currentPath={currentPath} path="/contacto" label="Contacto" />,
		<MenuItem key="preferencias" currentPath={currentPath} path="/preferencias" label="Preferencias" />
	];

  if (authHelper.isSuperusuario()) menuItems.push(<MenuItem key="admin" currentPath={currentPath} path="/admin" label="Administración" />);

	return (
		<nav className="navbar navbar-default">
			<div className="container-fluid">
				<div className="navbar-header">
					<button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span className="sr-only">Toggle navigation</span>
						<span className="icon-bar"></span>
						<span className="icon-bar"></span>
						<span className="icon-bar"></span>
					</button>
					<Link to="/inicio" className="navbar-brand">
						<img alt="Los Talas" src="/img/logo.png"/>&ensp;&ensp;Barrio Los Talas
					</Link>
				</div>
				<div className="collapse navbar-collapse" id="navbar">
					<ul className="nav navbar-nav">{menuItems}</ul>
					<ul className="nav navbar-nav navbar-right">
						<li>
							<a href="/login" onClick={logout}>
								Cerrar sesi&oacute;n de <strong>{username}</strong>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	);
};

Menu.propTypes = {
	JWT: React.PropTypes.object,
	redirect: React.PropTypes.func,
	requestJWTRevoke: React.PropTypes.func,
	routing: React.PropTypes.object
};

const mapDispatchToProps = function(dispatch) {
	return {
		requestJWTRevoke: (jwt) => requestJWTRevoke(jwt, fetch, dispatch),
		redirect: () => dispatch(push("/login"))
	};
};

const mapStateToProps = function (store) {
	return {
		JWT: store.security.JWT,
		routing: store.routing
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
