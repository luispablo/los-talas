import React from "react";
import { connect } from "react-redux";
import { requestLotes, requestLotesUsuarios } from "../../reducers/lotes";
import { clearUsuarios, requestGenerarCodigos, requestUpdateUsuario, requestUsuarios } from "../../reducers/usuarios";
import { AuthFetch } from "tokenauth";
import ListadoUsuarios from "./ListadoUsuarios";

class Admin extends React.Component {
  componentDidMount () {
    this.props.load(this.props.fetcher);
  }
  render () {
    return (
      <div className="panel panel-primary">
        <div className="panel-body">
          <button className="btn btn-primary" onClick={() => this.props.generarCodigos(this.props.fetcher)}>Generar c&oacute;digos</button>
          <ListadoUsuarios  usuarios={this.props.usuarios}
                            lotes={this.props.lotes}
                            lotesUsuarios={this.props.lotesUsuarios}
                            onUpdate={this.props.update(this.props.fetcher)} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = function (store) {
  return {
    fetcher: AuthFetch(store.security.JWT),
    lotes: store.lotes.lotes,
    lotesUsuarios: store.lotes.lotesUsuarios,
    usuarios: store.usuarios.items
  };
};

const mapDispatchToProps = function (dispatch) {
  return {
    generarCodigos (fetcher) {
      const that = this;
      requestGenerarCodigos(fetcher, dispatch).then(() => that.load(fetcher));
    },
    load (fetcher) {
      dispatch(clearUsuarios());
      requestLotes(fetcher, dispatch);
      requestLotesUsuarios(fetcher, dispatch);
      requestUsuarios(fetcher, dispatch);
    },
    update (fetcher) {
      return usuario => requestUpdateUsuario(usuario, fetcher, dispatch);
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Admin);
