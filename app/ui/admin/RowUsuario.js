import React from "react";
import _ from "lodash";

const LabelLote = function (props) {
  return (
    <span>
      <span className="label label-default">
        {props.lote.numero}&nbsp;
        <span className="glyphicon glyphicon-remove" aria-hidden="true"></span>
      </span>&nbsp;
    </span>
  );
};

class RowUsuario extends React.Component {
  constructor (props) {
    super(props);
    this.state = _.assign({}, props.usuario);
    this.undo = this.undo.bind(this);
    this.bajaLogica = this.bajaLogica.bind(this);
  }
  isDirty () {
    const s = this.state;
    const u = this.props.usuario;

    return s.nombre !== u.nombre || s.apellido !== u.apellido || s.email !== u.email || s.observaciones !== u.observaciones;
  }
  undo () {
    this.setState(this.props.usuario);
  }
  bajaLogica () {
    this.props.onUpdate(_.assign({}, this.state, { fecha_baja: new Date() }));
  }
  render () {
    const lotes = _.filter(this.props.lotes, l => _.some(this.props.lotesUsuarios, lu => lu.lote_id === l.id && lu.usuario_id === this.state.id));

    return (
      <tr className={this.state.fecha_baja ? "danger" : (this.isDirty() ? "warning" : "")}>
        <td><input type="text" className="form-control input-sm" value={this.state.nombre} onChange={e => this.setState({nombre: e.target.value})} /></td>
        <td><input type="text" className="form-control input-sm" value={this.state.apellido} onChange={e => this.setState({apellido: e.target.value})} /></td>
        <td><input type="text" className="form-control input-sm" value={this.state.email} onChange={e => this.setState({email: e.target.value})} /></td>
        <td><input type="text" className="form-control input-sm" value={this.state.codigo_especial || ""} onChange={e => this.setState({codigo_especial: e.target.value})} /></td>
        <td><input type="text" className="form-control input-sm" value={this.state.observaciones || ""} onChange={e => this.setState({observaciones: e.target.value})} /></td>
        <td>{lotes.map(l => <LabelLote key={l.id} lote={l} />)}</td>
        <td>
          <div className="btn-group" role="group">
            {this.isDirty() ?
              <button className="btn btn-success btn-xs" onClick={() => this.props.onUpdate(this.state)}><span className="glyphicon glyphicon-ok" aria-hidden="true"></span></button>
            : null}
            {this.isDirty() ?
              <button className="btn btn-default btn-xs" onClick={this.undo}><span className="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
            : null}
            <button className="btn btn-danger btn-xs" onClick={this.bajaLogica}><span className="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
          </div>
        </td>
      </tr>
    );
  }
};

export default RowUsuario;
