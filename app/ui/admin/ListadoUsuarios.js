import React from "react";
import RowUsuario from "./RowUsuario";

const ListadoUsuarios = function (props) {
  const items = props.usuarios.sort((u1, u2) => u1.id < u2.id ? -1 : 1).map(function (usuario) {
    return <RowUsuario key={usuario.id} usuario={usuario} onUpdate={props.onUpdate}
                        lotes={props.lotes} lotesUsuarios={props.lotesUsuarios} />;
  });

  return (
    <table className="table table-condensed table-striped">
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Apellido</th>
          <th>e-mail</th>
          <th>C&oacute;digo</th>
          <th>Observaciones</th>
          <th>Lotes</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
        {items}
      </tbody>
    </table>
  );
};

export default ListadoUsuarios;
