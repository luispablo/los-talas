import React from "react";
import Menu from "./Menu";
import PanelMessages from "bootstrap-feedback-panel";
import { connect } from "react-redux";

const Layout = (props) => {
	return (
		<div className="container-fluid club-house-back">
			<Menu />
			<div className="row">
				<div className="col-xs-12 col-md-offset-2 col-md-8 text-center">
					<PanelMessages messages={props.messages} />
				</div>
			</div>
			<div className="row">
				<div className="col-md-12">
					{props.children}
				</div>
			</div>
		</div>
	);
};

Layout.propTypes = {
	messages: React.PropTypes.array,
	children: React.PropTypes.object
};

const mapStateToProps = function (store) {
	return {
		messages: store.messages.items
	};
};

export default connect(mapStateToProps)(Layout);
