
var lotesRouter = function (knex, log) {

  var getAll = function (req, res) {
    knex("lotes").select().then(function (lotes) {
      res.json(lotes);
    }).catch(function (err) {
      log.error(err);
      res.status(err.code || 500).json(err);
    });
  };

  var getLotesUsuarios = function (req, res) {
    knex("lotes_usuario").select().then(function (lotesUsuarios) {
      res.json(lotesUsuarios);
    }).catch(function (err) {
      log.error(err);
      res.status(err.code || 500).json(err);
    });
  };

  return {
    getAll: getAll,
    getLotesUsuarios: getLotesUsuarios
  };
};

module.exports = lotesRouter;
