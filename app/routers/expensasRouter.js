var _ = require("lodash");

var expensasRouter = function (knex, log) {

  var getProveedores = function (req, res) {
    knex("proveedores").select().then(function (proveedores) {
      res.json(proveedores);
    }).catch(function (err) {
      log.error(err);
      res.status(err.code || 500).json(err);
    });
  };

  var getConceptos = function (req, res) {
    knex("conceptos").select().then(function (conceptos) {
      res.json(conceptos);
    }).catch(function (err) {
      log.error(err);
      res.status(err.code || 500).json(err);
    });
  };

  var getLiquidaciones = function (req, res) {
    knex("liquidaciones_expensas").select().then(function (liquidaciones) {
      res.json(liquidaciones);
    }).catch(function (err) {
      log.error(err);
      res.status(err.code || 500).json(err);
    });
  };

  var getGastosLiquidacion = function (req, res) {
    knex("gastos_liquidaciones").where("liquidacion_id", parseInt(req.params.id)).select().then(function (gastos) {
      res.json(gastos);
    }).catch(function (err) {
      log.error(err);
      res.status(err.code || 500).json(err);
    });
  };

  var updateGasto = function updateGasto (req, res) {
    knex("gastos_liquidaciones").where("id", parseInt(req.params.id)).update(req.body).then(function (updatedIds) {
      res.json(updatedIds);
    }).catch(function (err) {
      log.error(err);
      res.status(err.code || 500).json(err);
    });
  };

  var deleteGasto = function deleteGasto (req, res) {
    knex("gastos_liquidaciones").where("id", parseInt(req.params.id)).del().then(function (deletedCount) {
      res.json(deletedCount);
    }).catch(function (err) {
      log.error(err);
      res.status(err.code || 500).json(err);
    });
  };

  var insertGasto = function insertGasto (req, res) {
    knex("gastos_liquidaciones").insert(req.body).then(function (insertedId) {
      res.json(insertedId);
    }).catch(function (err) {
      log.error(err);
      res.status(err.code || 500).json(err);
    });
  };

  return {
    deleteGasto: deleteGasto,
    getConceptos: getConceptos,
    getGastosLiquidacion: getGastosLiquidacion,
    getLiquidaciones: getLiquidaciones,
    getProveedores: getProveedores,
    insertGasto: insertGasto,
    updateGasto: updateGasto
  };
};

module.exports = expensasRouter;
