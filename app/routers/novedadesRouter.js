
var novedadesRouter = function (knex, log) {

	function get (req, res) {
		knex("novedades").orderBy("fecha", "desc").select().then(function (novedades) {
			res.json(novedades);
		}).catch(function (err) {
			log.debug(err);
			res.status(err.code || 500).json(err);
		});
	}

	return {
		get: get
	};
};

module.exports = novedadesRouter;
