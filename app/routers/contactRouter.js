var jwt = require("jwt-simple");
var Mailgun = require("mailgun").Mailgun;

var contactRouter = function (knex, contactConfig, secret, log) {

	function receiveQuery (req, res) {
		var token = req.headers["x-access-token"]; // Only allow token in HTTP headers
		var decodedToken = jwt.decode(token, secret);

		knex("usuarios").where("email", decodedToken.iss).select().then(function (usuarios) {
			if (usuarios.length === 1) {
				var usuario = usuarios[0];
				var mg = new Mailgun(contactConfig.mailgunAPIKey);
				var sender = usuario.nombre +" "+ usuario.apellido +" <"+ usuario.email +">";

				mg.sendText(sender, contactConfig.targetMail, "Comisión Los Talas - Consulta", req.body.text, function (err) {
					if (err) {
						log.error(err);
						res.status(500).end();
					} else {
						res.status(200).end();
					}
				});
			} else {
				throw new Error("Se encontraron "+ usuarios.length +" para el e-mail "+ decodedToken.iss);
			}
		}).catch(function (err) {
			log.error(err);
			res.status(500).end();
		});
	}

	return {
		receiveQuery: receiveQuery
	};
};

module.exports = contactRouter;
