
var documentosRouter = function (knex, log) {

	function get (req, res) {
		var query = knex("documentos");

		if (req.query.tipo) query = query.where("tipo", req.query.tipo);

		query.select().then(function (documentos) {
			res.json(documentos);
		}).catch(function (err) {
			log.debug(err);
			res.status(err.code || 500).json(err);
		});
	}

	return {
		get: get
	};
};

module.exports = documentosRouter;
