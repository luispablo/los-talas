var _ = require("lodash");
var moment = require("moment");

var encuestasRouter = function (knex, log) {

  var putVoto = function putVoto (req, res) {
    var idEncuesta = req.params.id;
    var idUsuario = req.body.idUsuario;
    var idRespuesta = req.body.idRespuesta;

    knex("votos").where({ encuesta_id: idEncuesta, usuario_id: idUsuario }).then(function (votos) {
      if (votos && votos.length > 0) {
        knex("votos").update({ respuesta_id: idRespuesta }).where({ id: votos[0].id }).then(function () {
          res.json("ok");
        }).catch(function (err) {
          log.error(err);
          res.status(err.code || 500).json(err);
        });
      } else {
        knex("votos").insert({
          encuesta_id: idEncuesta, respuesta_id: idRespuesta, usuario_id: idUsuario
        }).then(function (newIds) {
          knex("lotes_usuario").where({ usuario_id: idUsuario }).then(function (lotes) {
            Promise.all(lotes.map(function (lote) {
              return knex("lotes_voto").insert({ voto_id: newIds[0], lote_id: lote.id });
            })).then(function () {
              res.json("ok");
            }).catch(function (err) {
              log.error(err);
              res.status(err.code || 500).json(err);
            });
          });
        }).catch(function (err) {
          log.error(err);
          res.status(err.code || 500).json(err);
        });
      }
    }).catch(function (err) {
      log.error(err);
      res.status(err.code || 500).json(err);
    });
  };

  var getEncuestas = function (req, res) {
    knex("encuestas").select().then(function (encuestas) {
      const queryRespuestas = encuestas.map(function (encuesta) {
        return new Promise(function (resolve, reject) {
          knex("respuestas").select().where({ encuesta_id: encuesta.id }).then(function (respuestas) {
            resolve(_.assign({}, encuesta, {
              respuestas: respuestas,
              estado: moment().isBetween(moment(encuesta.fecha_inicio), moment(encuesta.fecha_fin)) ? "EN_CURSO": "CERRADA"
            }));
          });
        });
      });

      Promise.all(queryRespuestas).then(function (encuestasConRespuestas) {
        const queryVotos = encuestasConRespuestas.map(function (encuesta) {
          return new Promise(function (resolve, reject) {
            knex("votos").where({ encuesta_id: encuesta.id }).then(function (votos) {
              resolve(_.assign({}, encuesta, { votos: votos }));
            });
          });
        })
        Promise.all(queryVotos).then(function (encuestasConVotos) {
          res.json(encuestasConVotos);
        }).catch(function (err) {
          log.error(err);
          res.status(err.code || 500).json(err);
        });
      }).catch(function (err) {
        log.error(err);
        res.status(err.code || 500).json(err);
      });
    }).catch(function (err) {
      log.error(err);
      res.status(err.code || 500).json(err);
    });
  };

  return {
    getEncuestas: getEncuestas,
    putVoto: putVoto
  };
};

module.exports = encuestasRouter;
