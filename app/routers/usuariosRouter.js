var bcrypt = require("bcryptjs");
var SendGrid = require('sendgrid');

var usuariosRouter = function (knex, contactConfig, log) {

  function resetPassword (req, res) {
    knex("usuarios").where("email", req.body.email).select().then(function (usuarios) {
      if (usuarios && usuarios.length > 0) {
        var usuario = usuarios[0];
        var nuevaClave = Math.random().toString(36).substring(7);
        knex("usuarios").where("id", usuario.id).update({ hash: bcrypt.hashSync(nuevaClave, 8)}).then(function () {
          var sender = new SendGrid.mail.Email(contactConfig.sender);
          var toEmail = new SendGrid.mail.Email(usuario.email);
          var subject = "Comisión Los Talas - Nueva contraseña";
          var contentText = "Hemos reseteado su contraseña para acceder al sitio. \nSu nueva contraseña es "+ nuevaClave;
          var content = new SendGrid.mail.Content("text/plain", contentText);
          var mail = new SendGrid.mail.Mail(sender, subject, toEmail, content);

          var sg = SendGrid(contactConfig.SendGridAPIKey);
          var request = sg.emptyRequest({method: "POST", path: "/v3/mail/send", body: mail.toJSON() });
          sg.API(request, function(error, response) {
            if (error) res.status(500).json(error);
            else res.json(response);
          });
        });
      } else {
        res.status(404).json("Usuario con e-mail "+ req.body.email +" no encontrado");
      }
    }).catch(function (err) {
      console.log(err);
      log.error(err);
      res.status(500).json(err);
    });
  }

  function generateHash (req, res) {
    if (req.query.codigo === "pinchatumadre") {
      var cantidadModificados = 0;

      knex("usuarios").whereNotNull("observaciones").select().then(function (usuarios) {
        usuarios.forEach(function (usuario) {
          knex("usuarios")
            .where("id", usuario.id)
            .update({ hash: bcrypt.hashSync(usuario.observaciones, 8), observaciones: null })
            .then(function () {
              if (++cantidadModificados === usuarios.length) res.json("listo");
            });
        });
      }).catch(function (err) {
        log.debug(err);
        res.status(500).json(err);
      });
    } else {
      res.json("codigo incorrecto");
    }
  }

  var generateSpecialCode = function (req, res) {
    knex.transaction(function (trx) {
      return trx("usuarios").whereNull("fecha_baja").select().then(function (usuarios) {
        return Promise.all(usuarios.map(function (usuario) {
          return trx("usuarios")
                  .where("id", usuario.id)
                  .update({ codigo_especial: Math.random().toString(36).substr(2, 8) });
        }));
      });
    }).then(function () {
      res.json("done");
    }).catch(function (err) {
      log.error(err);
      res.status(500).json(err);
    });
  };

  var getAll = function (req, res) {
    knex("usuarios").select().then(function (usuarios) {
      res.json(usuarios);
    }).catch(function (err) {
      log.error(err);
      res.status(500).json(err);
    });
  };

  var update = function (req, res) {
    knex("usuarios").where("id", parseInt(req.params.id)).update({
      nombre: req.body.nombre,
      apellido: req.body.apellido,
      email: req.body.email,
      codigo_especial: req.body.codigo_especial,
      observaciones: req.body.observaciones,
      fecha_baja: req.body.fecha_baja
    }).then(function (affectedRows) {
      res.json(affectedRows);
    }).catch(function (err) {
      log.error(err);
      res.status(500).json(err);
    });
  };

  return {
    generateHash: generateHash,
    generateSpecialCode: generateSpecialCode,
    getAll: getAll,
    resetPassword: resetPassword,
    update: update
  };
};

module.exports = usuariosRouter;
