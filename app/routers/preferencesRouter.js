var bcrypt = require("bcryptjs");

var CURRENT_PASSWORD_INCORRECT = "Current password incorrect";
var NO_UPDATED_ROWS = "No updated rows";

var preferencesRouter = function (knex, log) {

	return {
		changePassword: function (req, res) {
			var email = req.body.email;
			var currentPassword = req.body.currentPassword;
			var newPassword = req.body.newPassword;

			knex("usuarios").where("email", email).select().then(function (rows) {
				if (rows && rows.length > 0 && bcrypt.compareSync(currentPassword, rows[0].hash)) {
					var hash = bcrypt.hashSync(newPassword, 8);

					knex("usuarios").where("email", email).update({hash: hash}).then(function (updatedRows) {
						if (updatedRows && updatedRows > 0) {
							res.status(200).end();
						} else {
							log.debug(NO_UPDATED_ROWS);
							res.status(500).send(NO_UPDATED_ROWS);
						}
					}).catch(function (err) {
						log.debug(err);
						res.status(err.code || 500).json(err);
					});
				} else {
					log.debug(CURRENT_PASSWORD_INCORRECT);
					res.status(401).send(CURRENT_PASSWORD_INCORRECT);
				}
			}).catch(function (err) {
				log.debug(err);
				res.status(err.code || 500).json(err);
			});
		}
	};
};

module.exports = preferencesRouter;
