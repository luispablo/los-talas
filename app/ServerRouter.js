"use strict";

var express = require("express");

module.exports = function (authMiddleware, authRoutes, knex, contactConfig, secret, log) {
  var router = express.Router();
  var preferencesRouter = require("./routers/preferencesRouter")(knex, log);
  var documentosRouter = require("./routers/documentosRouter")(knex, log);
  var contactRouter = require("./routers/contactRouter")(knex, contactConfig, secret, log);
  var novedadesRouter = require("./routers/novedadesRouter")(knex, log);
  var usuariosRouter = require("./routers/usuariosRouter")(knex, contactConfig, log);
  var lotesRouter = require("./routers/lotesRouter")(knex, log);
  var expensasRouter = require("./routers/expensasRouter")(knex, log);
  var encuestasRouter = require("./routers/encuestasRouter")(knex, log);

  router.get("/auth/validate_token", authRoutes.validateToken);
  router.post("/auth/token", authRoutes.createToken());
  router.delete("/auth/token", authRoutes.deleteToken);

  router.put("/preferences/password", preferencesRouter.changePassword);

  router.post("/contact/receiveQuery", authMiddleware, contactRouter.receiveQuery);

  router.get("/documentos", authMiddleware, documentosRouter.get);

  router.get("/expensas/conceptos", authMiddleware, expensasRouter.getConceptos);
  router.get("/expensas/liquidaciones", authMiddleware, expensasRouter.getLiquidaciones);
  router.get("/expensas/liquidaciones/:id/gastos", authMiddleware, expensasRouter.getGastosLiquidacion);
  router.put("/expensas/gastos/:id", authMiddleware, expensasRouter.updateGasto);
  router.delete("/expensas/gastos/:id", authMiddleware, expensasRouter.deleteGasto);
  router.post("/expensas/gastos", authMiddleware, expensasRouter.insertGasto);
  router.get("/expensas/proveedores", authMiddleware, expensasRouter.getProveedores);

  router.get("/lotes", authMiddleware, lotesRouter.getAll);
  router.get("/lotes-usuarios", authMiddleware, lotesRouter.getLotesUsuarios);

  router.get("/novedades", authMiddleware, novedadesRouter.get);

  router.get("/usuarios", authMiddleware, usuariosRouter.getAll);
  router.put("/usuarios/generate-special-code", authMiddleware, usuariosRouter.generateSpecialCode);
  router.put("/usuarios/generate-hash", usuariosRouter.generateHash);
  router.put("/usuarios/reset-password", usuariosRouter.resetPassword);
  router.put("/usuarios/:id", authMiddleware, usuariosRouter.update);

  router.get("/encuestas", authMiddleware, encuestasRouter.getEncuestas);
  router.put("/encuestas/:id/votar", authMiddleware, encuestasRouter.putVoto);

  return router;
};
