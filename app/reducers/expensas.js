import { createAction, createReducer } from "redux-act";
import { commonReducersFetcher, messages } from "common-reducers";
import _ from "lodash";

const setConceptos = createAction("set conceptos");
const setLiquidaciones = createAction("set liquidaciones");
const setProveedores = createAction("set proveedores");
const clearGastos = createAction("clear gastos");
const addGastos = createAction("add gastos");
const removeGasto = createAction("remove gasto");
const replaceGasto = createAction("replace gasto");

const requestConceptos = function (fetcher, dispatch) {
	return commonReducersFetcher("requestConceptos", "/api/expensas/conceptos", {}, fetcher, dispatch)
          .then(conceptos => dispatch(setConceptos(conceptos)));
};

const requestProveedores = function (fetcher, dispatch) {
  return commonReducersFetcher("requestProveedores", "/api/expensas/proveedores", {}, fetcher, dispatch)
          .then(proveedores => dispatch(setProveedores(proveedores)));
};

const requestLiquidaciones = function (fetcher, dispatch) {
	return commonReducersFetcher("requestLiquidaciones", "/api/expensas/liquidaciones", {}, fetcher, dispatch)
          .then(liquidaciones => dispatch(setLiquidaciones(liquidaciones)));
};

const requestGastosLiquidacion = function (idLiquidacion, fetcher, dispatch) {
  const URL = `/api/expensas/liquidaciones/${idLiquidacion}/gastos`;  
  return commonReducersFetcher("requestGastosLiquidacion", URL, {}, fetcher, dispatch)
          .then(gastos => dispatch(addGastos(gastos)));
};

const requestUpdateGasto = function (gasto, fetcher, dispatch) {
  const URL = `/api/expensas/gastos/${gasto.id}`;
  return commonReducersFetcher("requestUpdateGasto", URL, { method: "PUT", body: JSON.stringify(gasto) }, fetcher, dispatch)
          .then(updatedIds => dispatch(replaceGasto(gasto)));
};

const requestDeleteGasto = function (idGasto, fetcher, dispatch) {
  const URL = `/api/expensas/gastos/${idGasto}`;
  return commonReducersFetcher("requestDeleteGasto", URL, { method: "DELETE" }, fetcher, dispatch)
          .then(deletedCount => dispatch(removeGasto(idGasto)));
};

const requestInsertGasto = function (gasto, fetcher, dispatch) {
  return commonReducersFetcher("requestInsertGasto", "/api/expensas/gastos", { method: "POST", body: JSON.stringify(gasto) }, fetcher, dispatch)
          .then(insertedId => dispatch(addGastos([_.assign({}, gasto, { id: insertedId })])));
};

export default createReducer({
  [addGastos]: (s, p) => _.assign({}, s, { gastos: s.gastos.concat(p) }),
  [clearGastos]: s => _.assign({}, s, { gastos: [] }),
  [removeGasto]: (s, id) => _.assign({}, s, { gastos: _.reject(s.gastos, { id: id })}),
  [replaceGasto]: (s, p) => _.assign({}, s, { gastos: _.reject(s.gastos, { id: p.id }).concat([p]) }),
  [setConceptos]: (s, p) => _.assign({}, s, { conceptos: p }),
  [setLiquidaciones]: (s, p) => _.assign({}, s, { liquidaciones: p }),
  [setProveedores]: (s, p) => _.assign({}, s, { proveedores: p })
}, { conceptos: [], liquidaciones: [], gastos: [], proveedores: [] });

export {
  addGastos,
  clearGastos,
  removeGasto,
  replaceGasto,
  requestConceptos,
  requestDeleteGasto,
  requestGastosLiquidacion,
  requestInsertGasto,
  requestLiquidaciones,
  requestProveedores,
  requestUpdateGasto,
  setConceptos,
  setLiquidaciones
};
