import { createAction, createReducer } from "redux-act";
import { commonReducersFetcher, messages } from "common-reducers";
import _ from "lodash";

const { addInfoMessage } = messages;

const clearEncuestas = createAction("clear encuestas");
const addEncuestas = createAction("add encuestas");

const requestGetEncuestas = function (fetcher, dispatch) {
	return commonReducersFetcher("requestGetEncuestas", "/api/encuestas", {}, fetcher, dispatch)
          .then(encuestas => dispatch(addEncuestas(encuestas)));
};

const requestVotar = function requestVotar (encuesta, respuesta, idUsuario, fetcher, dispatch) {
  return new Promise(function (resolve, reject) {
    const URL = `/api/encuestas/${encuesta.id}/votar`;
    const body = JSON.stringify({ idUsuario, idRespuesta: respuesta.id });
    fetcher(URL, { method: "PUT", body }).then(function (res) {
      if (res.status === 200) return res.json();
      else throw new Error(res);
    }).then(function () {
      dispatch(addInfoMessage(`Voto "${respuesta.valor}" grabado con éxito`));
      resolve();
    }).catch(err => reject(err));
  });
};

export default createReducer({
  [addEncuestas]: (s, p) => _.assign({}, s, { items: s.items.concat(p) }),
  [clearEncuestas]: s => _.assign({}, s, { items: [] }),
}, { items: [] });

export {
  addEncuestas,
  clearEncuestas,
  requestGetEncuestas,
  requestVotar
};
