import { createAction, createReducer } from "redux-act";
import { commonReducersFetcher, messages } from "common-reducers";
import _ from "lodash";

require("es6-promise").polyfill();

const clearUsuarios = createAction("clear usuarios");
const setUsuarios = createAction("set usuarios");
const replaceUsuario = createAction("replace usuario");

const requestUsuarios = function (fetcher, dispatch) {
	return commonReducersFetcher("requestUsuarios", "/api/usuarios", {}, fetcher, dispatch)
          .then(usuarios => dispatch(setUsuarios(usuarios)));
};
const requestUpdateUsuario = function (usuario, fetcher, dispatch) {
  const params = { method: "PUT", body: JSON.stringify(usuario) };
  return commonReducersFetcher("requestUpdateUsuario", `/api/usuarios/${usuario.id}`, params, fetcher, dispatch)
          .then(() => dispatch(replaceUsuario(usuario)));
};
const requestGenerarCodigos = function (fetcher, dispatch) {
  return commonReducersFetcher("requestGenerarCodigos", "/api/usuarios/generate-special-code", { method: "PUT" }, fetcher, dispatch);
};

export default createReducer({
  [clearUsuarios]: (state) => _.assign({}, state, { items: [] }),
  [replaceUsuario]: (s, p) => _.assign({}, s, { items: _.reject(s.items, { id: p.id }).concat(p) }),
	[setUsuarios]: (state, payload) => _.assign({}, state, { items: payload })
}, { items: [] });

export {
  clearUsuarios,
  replaceUsuario,
  requestGenerarCodigos,
  requestUpdateUsuario,
  requestUsuarios,
  setUsuarios
};
