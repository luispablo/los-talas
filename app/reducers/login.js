import { createAction, createReducer } from "redux-act";

export const setEMail = createAction("set login e-mail");
export const setPassword = createAction("set login password");

export default createReducer({
	[setEMail]: (state, payload) => Object.assign({}, state, { email: payload }),
	[setPassword]: (state, payload) => Object.assign({}, state, { password: payload })
}, { email: "", password: "" });
