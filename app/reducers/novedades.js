import { createAction, createReducer } from "redux-act";

require("es6-promise").polyfill();

export const setNovedades = createAction("set novedades");

export const requestNovedades = function (fetcher, dispatch) {
	return new Promise((resolve, reject) => {
		fetcher("/api/novedades").then(function (res) {
			if (res.status === 200) return res.json();
			else throw new Error("Error getting actas");
		}).then(function (novedades) {
			dispatch(setNovedades(novedades));
			resolve(novedades);
		}).catch(function (err) {
			console.error(err);
			reject(err);
		});
	});
};

export default createReducer({
	[setNovedades]: (state, payload) => Object.assign({}, state, { items: payload })
}, { items: [] });
