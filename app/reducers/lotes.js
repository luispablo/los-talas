import { createAction, createReducer } from "redux-act";
import { commonReducersFetcher, messages } from "common-reducers";
import _ from "lodash";

require("es6-promise").polyfill();

const setLotes = createAction("set lotes");
const setLotesUsuarios = createAction("set lotes usuarios");

const requestLotes = function (fetcher, dispatch) {
	return commonReducersFetcher("requestLotes", "/api/lotes", {}, fetcher, dispatch)
          .then(lotes => dispatch(setLotes(lotes)));
};

const requestLotesUsuarios = function (fetcher, dispatch) {
  return commonReducersFetcher("requestLotesUsuarios", "/api/lotes-usuarios", {}, fetcher, dispatch)
          .then(lotesUsuarios => dispatch(setLotesUsuarios(lotesUsuarios)));
};

export default createReducer({
	[setLotes]: (state, payload) => _.assign({}, state, { lotes: payload }),
	[setLotesUsuarios]: (state, payload) => _.assign({}, state, { lotesUsuarios: payload })
}, { lotes: [], lotesUsuarios: [] });

export {
  requestLotes,
  requestLotesUsuarios,
  setLotes,
  setLotesUsuarios
};
