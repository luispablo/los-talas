require("es6-promise").polyfill();

import { createAction, createReducer } from "redux-act";
import { application } from "common-reducers";

const { setFetchingFunction, removeFetchingFunction } = application;

export const clearQueryText = createAction("clear query text");
export const setQueryText = createAction("set query text");

export const sendQuery = function (queryText, fetcher, dispatch) {
	return new Promise((resolve, reject) => {
		dispatch(setFetchingFunction({sendQuery: 1}));

		fetcher("/api/contact/receiveQuery", {
			method: "POST",
			body: JSON.stringify({ text: queryText }),
			headers: { "Accept": "application/json", "Content-Type": "application/json" }
		}).then(function (res) {
			dispatch(removeFetchingFunction("sendQuery"));
			if (res.status === 200) {
				dispatch(clearQueryText());
				resolve();
			} else {
				reject(res);
			}
		}).catch(function (err) {
			dispatch(removeFetchingFunction("sendQuery"));
			reject(err);
		});
	});
};

export default createReducer({
	[clearQueryText]: (state) => Object.assign({}, state, { queryText: "" }),
	[setQueryText]: (state, payload) => Object.assign({}, state, { queryText: payload })
}, { queryText: "" });
