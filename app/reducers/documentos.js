import { createAction, createReducer } from "redux-act";

require("es6-promise").polyfill();

export const setDocumentos = createAction("set documentos");
export const select = createAction("select documento");

export const requestDocumentos = function (fetcher, dispatch) {
	return new Promise((resolve, reject) => {
		fetcher("/api/documentos").then(function (res) {
			if (res.status === 200) return res.json();
			else throw new Error("Error getting actas");
		}).then(function (documentos) {
			dispatch(setDocumentos(documentos));
			resolve(documentos);
		}).catch(function (err) {
			console.error(err);
			reject(err);
		});
	});
};

export default createReducer({
	[setDocumentos]: function (state, payload) {
		const selected = payload && payload.length > 0 ? payload[0] : null;
		return Object.assign({}, state, { items: payload, selected: selected });
	},
	[select]: (state, payload) => Object.assign({}, state, { selected: payload })
}, { items: [], selected: null });
