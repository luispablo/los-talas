require("es6-promise").polyfill();

export const changePassword = function (email, currentPassword, newPassword, fetcher) {
	return new Promise((resolve, reject) => {
		const options = {
			method: "PUT",
			headers: { "Accept": "application/json", "Content-Type": "application/json" },
			body: JSON.stringify({email, currentPassword, newPassword})
		};
		fetcher("/api/preferences/password", options).then(res => resolve(res)).catch(err => reject(err));
	});
};
