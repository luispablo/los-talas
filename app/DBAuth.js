var bcrypt = require("bcryptjs");
var _ = require("lodash");

var DBAuth = function (knex, log) {

	return {
		authenticate: function (username, password) {
			return new Promise(function (resolve, reject) {
				knex.select().table("usuarios").where("email", username).then(function (rows) {
					if (rows && rows.length > 0 && bcrypt.compareSync(password, rows[0].hash)) {
            var usuario = rows[0];
            if (!usuario.fecha_baja) {
              knex("perfiles")
                .innerJoin("perfiles_usuario", "perfiles.id", "perfiles_usuario.perfil_id")
                .where("perfiles_usuario.usuario_id", usuario.id)
                .then(function (perfiles) {
                  resolve(_.assign({}, usuario, { perfiles: perfiles }));
                });
            } else {
              reject({ code: 401, message: "Inactive user" });
            }
					} else {
						reject({ code: 401, message: "Wrong username or password" });
					}
				}).catch(function (err) {
					log.error(err);
					reject(err);
				});
			});
		}
	};
};

module.exports = DBAuth;
