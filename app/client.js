import React from "react"; // eslint-disable-line no-unused-vars
import ReactDOM from "react-dom";
import { createStore, combineReducers, compose, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import { browserHistory } from "react-router";
import { syncHistoryWithStore, routerMiddleware, routerReducer } from "react-router-redux";
import ClientRouter from "./ClientRouter";
import { application, messages, Security } from "common-reducers";
import login from "./reducers/login";
import documentos from "./reducers/documentos";
import contact from "./reducers/contact";
import novedades from "./reducers/novedades";
import usuarios from "./reducers/usuarios";
import expensas from "./reducers/expensas";
import encuestas from "./reducers/encuestas";
import lotes from "./reducers/lotes";
import moment from "moment";
import polyfills from "./helpers/polyfills";
import numeral from "numeral";

polyfills();
require("es6-promise").polyfill();

numeral.register("locale", "es", {
  delimiters: { thousands: ".", decimal: "," },
  abbreviations: {
    thousand: "k",
    million: "mm",
    billion: "b",
    trillion: "t"
  },
  ordinal: function (number) {
    var b = number % 10;
    return (b === 1 || b === 3) ? "er" :
          (b === 2) ? "do" :
          (b === 7 || b === 0) ? "mo" :
          (b === 8) ? "vo" :
          (b === 9) ? "no" : "to";
  },
  currency: { symbol: "$" }
});
numeral.locale("es");

window.moment = moment;

const { restoreJWT } = Security;

const reducers = combineReducers({
	application, contact, documentos, encuestas, expensas, login, messages, novedades, lotes,
  routing: routerReducer, security: Security(window.localStorage), usuarios
});
const routingMiddleware = routerMiddleware(browserHistory);
const reduxDevTools = window.devToolsExtension ? window.devToolsExtension() : f => f;
const store = createStore(reducers, compose(applyMiddleware(routingMiddleware), reduxDevTools));

const history = syncHistoryWithStore(browserHistory, store);
const divMain = document.getElementById("main");
const app = <Provider store={store}>{ClientRouter(history, store)}</Provider>;

store.dispatch(restoreJWT());

window.onload = function () {
	moment.locale("es");
	ReactDOM.render(app, divMain);
};
