
exports.up = function (knex) {
	return knex.schema.createTable("usuarios", function (table) {
		table.increments("id").primary();
		table.string("nombre", 50);
		table.string("apellido", 50);
		table.string("email", 80).unique();
		table.string("hash", 100);
		table.timestamps(true);
	});
};

exports.down = function (knex) {
	return knex.schema.dropTable("usuarios");
};
