
exports.up = function (knex) {
	return knex.schema.table("usuarios", function (table) {
		table.string("observaciones");
	});
};

exports.down = function (knex) {
	return knex.schema.table("usuarios", function (table) {
		table.dropColumn("observaciones");
	});
};
