
exports.up = function (knex) {
  return knex.schema.createTable("votos", function (table) {
    table.increments("id").primary();
    table.integer("encuesta_id").unsigned().index().references("id").inTable("encuestas");
    table.integer("respuesta_id").unsigned().index().references("id").inTable("respuestas");
    table.integer("usuario_id").unsigned().index().references("id").inTable("usuarios");
    table.text("comentario");
    table.timestamps();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("votos");
};
