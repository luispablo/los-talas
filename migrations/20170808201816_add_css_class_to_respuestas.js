
exports.up = function (knex) {
  return knex.schema.table("respuestas", function (table) {
    table.string("css_class", 100);
  });
};

exports.down = function (knex) {
  return knex.schema.table("respuestas", function (table) {
    table.dropColumn("css_class");
  });
};
