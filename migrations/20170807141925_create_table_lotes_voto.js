
exports.up = function (knex) {
  return knex.schema.createTable("lotes_voto", function (table) {
    table.increments("id").primary();
    table.integer("voto_id").unsigned().index().references("id").inTable("votos");
    table.integer("lote_id").unsigned().index().references("id").inTable("lotes");
    table.timestamps();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("lotes_voto");
};
