
exports.up = function (knex) {
  return knex.schema.createTable("liquidaciones_expensas", function (table) {
    table.increments("id").primary();
    table.integer("mes");
    table.integer("anio");
    table.timestamps();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("liquidaciones_expensas");
};
