
exports.up = function(knex, Promise) {
	return knex.schema.createTable("perfiles", function (table) {
		table.increments("id").primary();
		table.string("codigo", 20);
		table.string("nombre", 50);
		table.timestamps(true);
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable("perfiles");
};
