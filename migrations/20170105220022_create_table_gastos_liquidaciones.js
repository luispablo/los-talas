
exports.up = function (knex) {
  return knex.schema.createTable("gastos_liquidaciones", function (table) {
    table.increments("id").primary();
		table.integer("liquidacion_id").unsigned().index().references("id").inTable("liquidaciones_expensas");
		table.integer("concepto_id").unsigned().index().references("id").inTable("conceptos");
    table.string("detalle", 250);
    table.decimal("importe");
    table.timestamps();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("gastos_liquidaciones");
};
