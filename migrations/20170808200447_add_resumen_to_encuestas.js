
exports.up = function (knex) {
  return knex.schema.table("encuestas", function (table) {
    table.text("resumen");
  });
};

exports.down = function (knex) {
  return knex.schema.table("encuestas", function (table) {
    table.dropColumn("resumen");
  });
};
