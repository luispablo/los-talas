
exports.up = function (knex) {
  return knex.schema.createTable("proveedores", function (table) {
    table.increments("id").primary();
    table.string("nombre", 150);
    table.timestamps();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("proveedores");
};
