
exports.up = function (knex) {
  return knex.schema.createTable("respuestas", function (table) {
    table.increments("id").primary();
    table.integer("encuesta_id").unsigned().index().references("id").inTable("encuestas");
    table.string("valor", 250);
    table.integer("orden");
    table.timestamps();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("respuestas");
};
