
exports.up = function(knex, Promise) {
	return knex.schema.createTable("lotes_usuario", function (table) {
		table.increments("id").primary();
		table.integer("lote_id").unsigned().index().references("id").inTable("lotes");
		table.integer("usuario_id").unsigned().index().references("id").inTable("usuarios");
		table.timestamps();
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable("lotes_usuario");
};
