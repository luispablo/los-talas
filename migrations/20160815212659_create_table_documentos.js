
exports.up = function (knex) {
	return knex.schema.createTable("documentos", function (table) {
		table.increments("id").primary();
		table.string("tipo", 20);
		table.string("descripcion", 50);
		table.string("url", 100);
		table.timestamps(true);
	});
};

exports.down = function (knex) {
	return knex.schema.dropTable("documentos");
};
