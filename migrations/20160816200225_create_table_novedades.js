
exports.up = function (knex) {
	return knex.schema.createTable("novedades", function (table) {
		table.increments("id").primary();
		table.date("fecha");
		table.string("titulo", 250);
		table.text("descripcion");
		table.timestamps(true);
	});
};

exports.down = function (knex) {
	return knex.schema.dropTable("novedades");
};
