
exports.up = function(knex, Promise) {
	return knex.schema.createTable("perfiles_usuario", function (table) {
		table.increments("id").primary();
		table.integer("perfil_id").unsigned().index().references("id").inTable("perfiles");
		table.integer("usuario_id").unsigned().index().references("id").inTable("usuarios");
		table.timestamps();
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable("perfiles_usuario");
};
