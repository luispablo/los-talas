
exports.up = function (knex) {
	return knex.schema.table("usuarios", function (table) {
		table.string("codigo_especial", 150);
	});
};

exports.down = function (knex) {
	return knex.schema.table("usuarios", function (table) {
		table.dropColumn("codigo_especial");
	});
};
