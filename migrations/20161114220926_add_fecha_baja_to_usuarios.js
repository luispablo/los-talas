
exports.up = function (knex) {
	return knex.schema.table("usuarios", function (table) {
		table.date("fecha_baja");
	});
};

exports.down = function (knex) {
	return knex.schema.table("usuarios", function (table) {
		table.dropColumn("fecha_baja");
	});
};
