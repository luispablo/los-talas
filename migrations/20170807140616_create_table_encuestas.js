
exports.up = function (knex) {
  return knex.schema.createTable("encuestas", function (table) {
    table.increments("id").primary();
    table.string("titulo", 255);
    table.text("detalle");
    table.date("fecha_inicio");
    table.date("fecha_fin");
    table.boolean("permite_comentarios");
    table.timestamps();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("encuestas");
};
