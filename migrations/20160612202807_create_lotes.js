
exports.up = function(knex, Promise) {
	return knex.schema.createTable("lotes", function (table) {
		table.increments("id").primary();
		table.integer("numero");
		table.timestamps(true);
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable("lotes");
};
