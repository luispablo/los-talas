
exports.up = function (knex) {
  return knex.schema.table("gastos_liquidaciones", function (table) {
    table.integer("proveedor_id").unsigned().index().references("id").inTable("proveedores");
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.table("gastos_liquidaciones", function (table) {
    table.dropColumn("proveedor_id");
  });
};
