
exports.up = function (knex) {
  return knex.schema.createTable("conceptos", function (table) {
    table.increments("id").primary();
    table.string("tipo", 30);
    table.string("nombre", 150);
    table.timestamps();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("conceptos");
};
