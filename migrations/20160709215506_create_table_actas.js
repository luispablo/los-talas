
exports.up = function(knex, Promise) {
	return knex.schema.createTable("actas", function (table) {
		table.increments("id").primary();
		table.string("descripcion", 50);
		table.string("url", 100);
		table.timestamps(true);
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable("actas");
};
